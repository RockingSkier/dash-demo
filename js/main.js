function setupVideo(url) {

    // URL to steam (MPD file).  Defaults to BBC Big Buck Bunny stream
    url = url || "http://rdmedia.bbc.co.uk/dash/ondemand/bbb/avc3/1/client_manifest-common_init.mpd";

    // Dash context
    var context = new Dash.di.DashContext();

    // Player object
    var player = new MediaPlayer(context);

    // Start the player
    player.startup();

    // Put the player in the page
    player.attachView(dashVideo);

    // Set the URL for the player
    player.attachSource(url);
}

// Add a class to an element
function addClass(el, className) {
    if (el.classList)
        el.classList.add(className);
    else
        el.className += ' ' + className;
}

// Remove a class from an element
function removeClass(el, className) {
    if (el.classList)
        el.classList.remove(className);
    else
        el.className = el.className.replace(new RegExp('(^| )' + className.split(' ').join('|') + '( |$)', 'gi'), ' ');
}

// Set the text of an element
function setText (el, string) {
    if (el.textContent !== undefined)
        el.textContent = string;
    else
        el.innerText = string;
}

// Button to start playback
var dashVideoContainer = document.getElementById("dashVideoContainer"),
    dashVideo = document.getElementById("dashVideo"),
    toggleDashVideoPlayback = document.getElementById("toggleDashVideoPlayback"),
    setUpDashVideo = document.getElementById('setUpDashVideo');

// On click event
setUpDashVideo.addEventListener('click', function (evt) {
    evt.preventDefault();

    var el = evt.target,
        url = el.dataset.videoUrl;

    removeClass(dashVideoContainer, 'hide');

    // Set up the video
    setupVideo(url);

    addClass(setUpDashVideo, 'hide');
});

toggleDashVideoPlayback.addEventListener('click', function () {
    if (dashVideo.paused) {
        dashVideo.play();
        setText(toggleDashVideoPlayback, 'Pause');
    } else {
        dashVideo.pause();
        setText(toggleDashVideoPlayback, 'Play');
    }
});

